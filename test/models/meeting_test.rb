require 'test_helper'

class MeetingTest < ActiveSupport::TestCase

  setup do
    @meeting = create(:meeting)
  end

  attr_accessor :meeting

  test "active_participant scope return only participants that are in the meeting" do
    meeting.add_participant
    meeting.add_participant
    meeting.add_participant
    participation = Participation.last
    participation.present = false
    participation.save
    assert_equal 3, meeting.participants.count
    assert_equal 2, meeting.active_participants.count
  end

  test "add_participant create a new participation" do
    assert_difference('Participation.count') do
      meeting.add_participant
    end
  end

  test "remove_participant make one participation inactive" do
    meeting.add_participant
    meeting.add_participant
    meeting.add_participant
    assert_difference('Participation.active.count', -1) do
      meeting.remove_participant
    end
  end

  test "remove_participant make and active participation inactive" do
    meeting.add_participant
    meeting.add_participant
    meeting.add_participant
    Participation.first.update!(:present => false)
    assert_difference('Participation.active.count', -1) do
      meeting.remove_participant
    end
  end

end
