require 'test_helper'

class ParticipationTest < ActiveSupport::TestCase

  test "scope active return only participant where present is true " do
    create_list(:participation, 2, :active)
    create_list(:participation, 3, :inactive)
    assert_equal 2, Participation.active.count
  end

end
