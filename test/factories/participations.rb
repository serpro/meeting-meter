require 'ffaker'

FactoryGirl.define do

  factory :participation do
    present true
  end 

  trait :active do
    present true
  end 

  trait :inactive do
    present false
  end

end
