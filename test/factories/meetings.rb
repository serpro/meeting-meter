require 'ffaker'

FactoryGirl.define do

  factory :meeting do
    title { FFaker::Lorem.words.to_s }
  end 

end
