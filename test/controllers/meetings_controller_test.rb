require 'test_helper'

class MeetingsControllerTest < ActionController::TestCase
  setup do
    @meeting = create(:meeting)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:meetings)
  end

  test "should paginate meetings" do
    Meeting.delete_all
    create_list(:meeting, 21)
    get :index, :page => 2
    assert_equal 1, assigns(:meetings).length
  end

  test "should get new" do
    get :new
    assert_response :redirect
  end

  test "should create an meeting in new action" do
    assert_difference('Meeting.count') do
      get :new
    end
  end

  test "should create an meeting with one participant in new action" do
    assert_difference('Participation.count') do
      get :new
    end
  end

  test "should show meeting" do
    get :show, id: @meeting
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @meeting
    assert_response :success
  end

  test "should remove a participant of a meeting" do
    @meeting.add_participant
    @meeting.add_participant
    @meeting.add_participant
    assert_equal 3, @meeting.active_participants.count
    xhr :delete, :manage_participant, id: @meeting
    assert_equal 2, @meeting.active_participants.count
  end

  test "should add a participant of a meeting" do
    @meeting.add_participant
    @meeting.add_participant
    @meeting.add_participant
    xhr :post, :manage_participant, id: @meeting
    assert_equal 4, @meeting.participants.count
  end

  test "should redirect after finish a meeting" do
    post :finish, id: @meeting
    assert_response :redirect
  end

  test "should define finish date on meetings as current time" do
    assert_nil @meeting.finish
    post :finish, id: @meeting
    @meeting.reload
    assert_not_nil @meeting.finish
  end

  test "should define all participations as inactives after finish a meeting" do
    @meeting.add_participant
    @meeting.add_participant
    @meeting.add_participant
    post :finish, id: @meeting
    @meeting.reload
    assert_equal [], @meeting.active_participants
  end

end
