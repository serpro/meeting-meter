class CreateMeetingsParticipants < ActiveRecord::Migration
  def change
    create_table :meetings_participants do |t|
      t.references :meeting
      t.references :participant
      t.boolean :present, :default => true
      t.timestamps null: false
    end
  end
end
