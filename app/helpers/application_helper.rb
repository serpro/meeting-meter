module ApplicationHelper

  def button(content, options = {})
    color = options[:color] || 'primary'
    content_tag(:button, content, :type => 'button', :class => "btn btn-#{color} btn-lg")
  end

  def plus_button
    content_tag(
      :button, 
      content_tag(:i, '' , :class => 'fa fa-plus'), 
      :type => 'button', :class => 'btn btn-success btn-circle btn-xl'
    )
  end

  def minus_button
    content_tag(
      :button, 
      content_tag(:i, '' , :class => 'fa fa-minus'), 
      :type => 'button', :class => 'btn btn-danger btn-circle btn-xl'
    )
  end

  def panel_information(title,amount,options = {})
    color = options[:color] || 'green'
    icon = options[:icon] || 'fa-tasks'
    content_tag(:div,
      content_tag(:div,
        content_tag(:div, 
          content_tag(:div,
            content_tag(:div, 
              content_tag(:i, '', :class => "fa #{icon} fa-5x"),
              :class => 'col-xs-3'
            ) +
            content_tag(:div, 
              content_tag(:div, amount, :class => "huge"),
              :class => 'col-xs-9 text-right'
            ),
            :class => 'row'
          )
        ),
        :class => 'panel-heading'
      ) +
      content_tag(:div, 
        content_tag(:span, title, :class => 'detail'),
        :class => 'panel-footer'
      ),
      :class => "panel panel-#{color}"
    )

  end  


end
