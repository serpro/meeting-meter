class MeetingsController < ApplicationController
  before_action :set_meeting, only: [:show, :edit, :finish, :manage_participant]

  def index
    @meetings = Meeting.page(params[:page])
  end

  def show
  end

  def new
    meeting = Meeting.new
    meeting.participants << Participant.new
    meeting.save!
    redirect_to edit_meeting_path(meeting)
  end

  # GET /meetings/1/edit
  def edit
  end

  def manage_participant
    @meeting.add_participant if request.post?
    @meeting.remove_participant if request.delete?
  end

  def finish
    @meeting.close!
    redirect_to meeting_path(@meeting)
  end

  def dashboard
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_meeting
      @meeting = Meeting.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def meeting_params
      params.require(:meeting).permit(:title)
    end
end
