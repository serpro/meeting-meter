class Participant < ApplicationRecord
  has_many :participations
  has_many :meetings, :through => :participations
end
