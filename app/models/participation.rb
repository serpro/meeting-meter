class Participation < ApplicationRecord
  scope :active, lambda { where(:present => true) }
  #FIXME make this test
  scope :current_month, lambda { where(created_at: Time.now.beginning_of_month..Time.now.end_of_month) }
  scope :today, lambda { where(created_at: Time.now.beginning_of_day..Time.now.end_of_day) }
 

  self.table_name = "meetings_participants"

  belongs_to :participant
  belongs_to :meeting

end
