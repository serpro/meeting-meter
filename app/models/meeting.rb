class Meeting < ApplicationRecord

  has_many :participations
  has_many :participants, :through => :participations
  has_many :active_participants, -> {Participation.active}, through: :participations, :source => :participant
  #FIXME make this test
  scope :current_month, lambda { where(created_at: Time.now.beginning_of_month..Time.now.end_of_month) }
  scope :today, lambda { where(created_at: Time.now.beginning_of_day..Time.now.end_of_day) }

  def add_participant
    self.participants << Participant.new
  end

  def remove_participant
    participation = self.participations.active.first
    participation.update!(present: false) unless participation.nil?
  end

  scope :duration, lambda { sum(:duration) }


  #FIXME make this test
  def duration
    ((self.finish || Time.now) - self.created_at).to_i
  end

  #FIXME make this test
  def self.time_for_humans(time)
    return0 if time.nil?
    d = time
    d = d.sum if d.is_a?(Array)
    "#{d.in_hours.to_i.to_s.rjust(2, '0')}:#{d.in_minutes.to_i.to_s.rjust(2, '0')}:#{d.in_seconds.to_i.to_s.rjust(2, '0')}"
  end

  #FIXME make this test
  def spent_time_by_participant
    self.participations.map do |participation|
      (participation.updated_at - participation.created_at).to_i
    end
  end

  #FIXME make this test
  def all_time_spent_by_participants
    self.spent_time_by_participant.sum
  end

  #FIXME make this test
  def cost
    (self.all_time_spent_by_participants.to_f/60/60) * self.hour_cost
  end

  # FIXME make this test
  def close!
    Meeting.transaction do
      now = DateTime.now.in_time_zone
      self.finish = now
      self.participations.active.update_all(:present => false, :updated_at => now)
      self.save!
    end
  end

  protected

  def hour_cost
    100
  end

end
